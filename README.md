### A simple website using vanilla JS + three.js library

This is an optimized built version of the website that is located at https://jojo-master-public.gitlab.io/three-js/  

Source code is located here:  https://gitlab.com/jojo-master-public/three-js/-/tree/dev  
