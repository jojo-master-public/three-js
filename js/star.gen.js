// Stars
function generate(starGeo, r, g, b) {

    let mat;

    switch (THREE.MathUtils.randInt(0, 2)) {
        case 0:
            mat = r;
            break;
        case 1:
            mat = g;
            break;
        case 2:
            mat = b;
            break;
    
        default:
            break;
    }

    const star = new THREE.Mesh(starGeo, mat);

    const [x, z]= Array(2)
        .fill()
        .map(() => THREE.MathUtils.randFloatSpread(100));

    star.position.set(x, 0.0, z);
    return star;
}

function generateStars(count) {
    const starGeo = new THREE.SphereGeometry(0.04, 8, 8);
    const starMatR = new THREE.MeshBasicMaterial({color: 0xffbbbb});
    const starMatG = new THREE.MeshBasicMaterial({color: 0xbbffbb});
    const starMatB = new THREE.MeshBasicMaterial({color: 0xbbbbff});

    var stars = Array(count).fill().map(() => generate(starGeo, starMatR, starMatG, starMatB));

    return stars;
}

function getRotationMatrix(time) {
    let m = new THREE.Matrix4(); 
    return m.makeRotationY(3.14 * time);
}

export { generateStars, getRotationMatrix, generate }