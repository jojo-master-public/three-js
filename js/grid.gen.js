function createGrid(root, size, spacing, cellSize, cellOffset, globalOffset){
    let cellGeo = new THREE.BoxGeometry(cellSize.x, cellSize.y, cellSize.z, 1, 1, 1);

    let cells = Array();

    for (let x = 0; x < size; x++) {
        for (let y = 0; y < size; y++) {

            if(THREE.MathUtils.randFloatSpread(100) > 0)
                continue;

            let rndColorIndex = THREE.MathUtils.randInt(0, 2);
            let rndColor = null;

            switch (rndColorIndex) {
                case 0:
                    rndColor = 0xbbbbff;
                    break;
                case 1:
                    rndColor = 0xffbbbb;
                    break;
                case 2:
                    rndColor = 0xbbffbb;
                    break;
                default:
                    break;
            }

            let cellMat = new THREE.MeshPhongMaterial({color: rndColor});
            let cell = new THREE.Mesh(cellGeo, cellMat);
            
            var randomOffsetY = THREE.MathUtils.randFloatSpread(cellSize.y);

            cell.position.set(x * cellOffset.x + x * spacing + globalOffset.x, randomOffsetY, y * cellOffset.z + y * spacing + globalOffset.z);
            root.add(cell);
            cells.push(cell);
        }
    }

    return cells;
}

function createCircle(root, size, count, spacing, cellSizeMin, cellSizeMax){

  let cells = Array();
  let rMat = new THREE.MeshPhongMaterial({color: 0xffbbbb});
  let gMat = new THREE.MeshPhongMaterial({color: 0xbbffbb});
  let bMat = new THREE.MeshPhongMaterial({color: 0xbbbbff});

  for (let i = 0; i < count; i++) {

    let x = THREE.MathUtils.randFloat(cellSizeMin.x, cellSizeMax.x);
    let y = THREE.MathUtils.randFloat(cellSizeMin.y, cellSizeMax.y);
    let z = THREE.MathUtils.randFloat(cellSizeMin.z, cellSizeMax.z);
    let cellGeo = new THREE.BoxGeometry(x, y, z, 1, 1, 1);

    let cellMat;
    let v = new THREE.Vector3(0.0, size, 0.0);

    let rndColorIndex = THREE.MathUtils.randInt(0, 2);
    switch (rndColorIndex) {
        case 0:
            cellMat = rMat;
            break;
        case 1:
            cellMat = gMat;
            break;
        case 2:
            cellMat = bMat;
            break;
        default:
            break;
    }
  
    let t = i / count;
    let m = new THREE.Matrix4();

    m = m.makeRotationZ(3.14 * t * 2);
    v.applyMatrix4(m);

    let cell = new THREE.Mesh(cellGeo, cellMat);
    cell.position.set(v.x, v.y, v.z);
    cell.lookAt(new THREE.Vector3(0.0, 0.0, 0.0));

    root.add(cell);
    cells.push(cell);
  }
  
  return cells;
}

export { createGrid, createCircle }