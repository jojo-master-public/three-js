import { EffectComposer } from 'three/addons/postprocessing/EffectComposer.js';
import { RenderPass } from 'three/addons/postprocessing/RenderPass.js';
import { UnrealBloomPass } from 'three/addons/postprocessing/UnrealBloomPass.js';
import { ShaderPass } from 'three/addons/postprocessing/ShaderPass.js';
import { ChromaticShader } from './ChromaticPass';
import { PixelizeShader } from './PixelizeShader';

var renderer;
var camera;
var scene;
var composer;
var renderPass;
var bloomPass;
var chromaticPass;
var chromaticShader;
var pixelizePass;
var pixelizeShader;

const ls = window.localStorage;

let ls_chromatic = ls.getItem('chromatic');
let ls_bloom = ls.getItem('bloom');
let ls_pixelize = ls.getItem('pixelize');

var chromatic = (ls_chromatic !== null) ? (ls_chromatic === 'true') : true;
var bloom = (ls_bloom !== null) ? (ls_bloom === 'true') : true;
var pixelize = (ls_pixelize !== null) ? (ls_pixelize === 'true') : true;



function initPostProcess(rend, cam, sc) {
    renderer = rend;
    camera = cam;
    scene = sc;

    pixelizeShader = new PixelizeShader();
    chromaticShader = new ChromaticShader();
    renderPass = new RenderPass(scene, camera);
    bloomPass = new UnrealBloomPass();
    bloomPass.copyUniforms.opacity.value = 0.507;

    let chromaticToggle = document.querySelector('#chromatic');
    chromaticToggle.checked = chromatic;
    chromaticToggle.addEventListener('change', onChromaticChanged);

    let bloomToggle = document.querySelector('#bloom');
    bloomToggle.checked = bloom;
    bloomToggle.addEventListener('change', onBloomChanged);

    let pixToggle = document.querySelector('#pixelize');
    pixToggle.checked = pixelize;
    pixToggle.addEventListener('change', onPixelizeChanged);

    onPostProcessChanged();
}

function onBloomChanged() {
    bloom = !bloom;

    ls.setItem('bloom', bloom);
    onPostProcessChanged();
}

function onPixelizeChanged() {
    pixelize = !pixelize;

    ls.setItem('pixelize', pixelize);
    onPostProcessChanged();
}

function onChromaticChanged() {
    chromatic = !chromatic;

    ls.setItem('chromatic', chromatic);
    onPostProcessChanged();
}

function onPostProcessChanged() {
    composer = new EffectComposer(renderer);
    composer.addPass(renderPass);

    if(pixelize) {
        pixelizePass = new ShaderPass(pixelizeShader);
        composer.addPass(pixelizePass);
    }

    if(bloom) {
        composer.addPass(bloomPass);
    }

    if(chromatic)
    {
        chromaticPass = new ShaderPass(chromaticShader);
        chromaticPass.renderToScreen  = true;
        composer.addPass(chromaticPass);
    }
}

function render() {
    composer?.render();
}

export { render, initPostProcess, onPostProcessChanged };