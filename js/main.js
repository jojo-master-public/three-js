// Initial setup
import { Vector2, Clock, Scene, Group, PerspectiveCamera, WebGLRenderer, AnimationMixer, MathUtils as MathF, BufferGeometry, Float32BufferAttribute, Points, Vector3, Int32BufferAttribute, Raycaster, BoxGeometry, MeshBasicMaterial, Mesh} from 'three';
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';
import { ParticlesShader } from './ParticlesBuffer';
import * as PP from './PostProcess'

let mouse;
let clock;
let sceneRoot;
let scene;
let canvas;
let camera;
let deltaTime = 0.0;

let roomMesh;
let roomAnimation;
let particlesShader;
let time = 0.0;
let raycastTimer = 0.0;
let raycastDelay = 0.05;
let debug_raycast;
let interactionSettings = {
  interactions: undefined,
  currentInteraction: undefined,
  cameraPosition: undefined,
  cameraOrigin: undefined,
  infoDiv: undefined,
  hoveredInteraction: undefined
}



function start() {
  let renderer;
  mouse = new Vector2();
  clock = new Clock();
  scene = new Scene();
  sceneRoot = new Group();
  deltaTime = clock.getDelta();

  camera = new PerspectiveCamera(60, window.innerWidth / window.innerHeight, 0.1, 1000);

  let rotX = MathF.DEG2RAD * -15;

  camera.position.setZ(235);
  camera.rotation.set(rotX, 0, 0);

  canvas = document.querySelector('#bg');
  renderer = new WebGLRenderer({
    canvas: canvas, alpha: true, antialias: true
  });

  loadRoomMesh();
  
  document.addEventListener('mousemove', onMouseMove, false);
  document.addEventListener('click', onMouseClick, false);

  scene.add(sceneRoot);

  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.setClearColor(0x020204);

  createParticles();
  createInteractions();

  PP.initPostProcess(renderer, camera, scene);
}

function createParticles() {
  let bufferGeometry = new BufferGeometry();
  particlesShader = new ParticlesShader();
  let radius = 120;

  let positions = [];
  let ids = [];
  let offset = new Vector3(-10, -55, -10);

  for (let i = 0; i < 200; i++) {
    let index = i;

    ids.push(index);
    
    positions.push((Math.random() * 2 - 1) * radius + offset.x);
    positions.push((Math.random() * 2 - 1) * radius + offset.y);
    positions.push((Math.random() * 2 - 1) * radius + offset.z);
  }

  bufferGeometry.setAttribute('position', new Float32BufferAttribute( positions, 3));
  bufferGeometry.setAttribute('id', new Int32BufferAttribute( ids, 1));

  let particleSystem = new Points(bufferGeometry, particlesShader);
  sceneRoot.add(particleSystem);
}

function loadRoomMesh() {
    let meshLoader = new GLTFLoader();
    meshLoader.load('assets/the_magic_room.glb', (gltf) => {
    roomMesh = gltf.scene;

    let roomYRot = MathF.DEG2RAD * -45;
    roomMesh.position.set(0, -125, 0);
    roomMesh.rotation.set(0, roomYRot, 0);

    roomAnimation = new AnimationMixer(roomMesh);

    let animations = gltf.animations;
    
    animations.forEach(( clip ) => {
      let anim = roomAnimation.clipAction( clip );
      anim.play();
    } );
    roomAnimation.animations = animations;

    sceneRoot.add(roomMesh);
  });
}

function handleCameraPosition() {
  if(camera !== undefined && interactionSettings.cameraPosition !== undefined) {

    let a = camera.position.clone();
    let b = interactionSettings.cameraPosition;

    let x = MathF.lerp(a.x, b.x, 0.05);
    let y = MathF.lerp(a.y, b.y, 0.035);
    let z = MathF.lerp(a.z, b.z, 0.05);

    camera.position.set(x, y, z);
  }
}

//#region INTERACTIONS

function handleInteractionClick(interaction) {
  let cn = interactionSettings.infoDiv.className;

  if(interaction === undefined) {
    interactionSettings.cameraPosition = interactionSettings.cameraOrigin.clone();
      interactionSettings.infoDiv.innerHTML = "";
      interactionSettings.currentInteraction = undefined;

    if(cn.includes(' anim-from-0-to-1')) {
      cn = cn.replace(' anim-from-0-to-1', ' anim-from-1-to-0');
      interactionSettings.infoDiv.className = cn;
    }
    return;
  } else {
    
    if(!cn.includes(' anim-from-0-to-1')) {
      let newCN = String.prototype.concat(cn, ' anim-from-0-to-1');
      interactionSettings.infoDiv.className = newCN;
    } else {
      cn = cn.replace(' anim-from-0-to-1', ' anim-from-1-to-0');
      interactionSettings.infoDiv.className = cn;
    }

    if(cn.includes(' anim-from-1-to-0')) {
      cn = cn.replace(' anim-from-1-to-0', ' anim-from-0-to-1');
      interactionSettings.infoDiv.className = cn;
    }
  }

  if(interactionSettings.currentInteraction === interaction) {
    handleInteractionClick(undefined);
  } else {
    // set camera position to currentInteraction.position + offset;
    if(interaction !== undefined) {
      let forward = new Vector3();
      camera.getWorldDirection(forward);
      let offset = new Vector3(-forward.x, -forward.y, -forward.z);
      offset.multiplyScalar(65.0);

      let p = interaction.position;

      interactionSettings.currentInteraction = interaction;
      interactionSettings.cameraPosition = new Vector3(p.x + offset.x, p.y + offset.y, p.z + offset.z);
      interactionSettings.infoDiv.innerHTML = interaction['info'];
    }
  }
}

function handleInteractionHover(interaction) {
  if(interaction === undefined && interactionSettings.hoveredInteraction !== undefined) {
    interactionSettings.hoveredInteraction.material = new MeshBasicMaterial({color: 0x00ff00, transparent: true, opacity: 0.0});
    interactionSettings.hoveredInteraction = undefined;
  } else {
    if(interactionSettings.hoveredInteraction !== undefined) {
      interactionSettings.hoveredInteraction.material = new MeshBasicMaterial({color: 0x00ff00, transparent: true, opacity: 0.0});
    }
    
    interactionSettings.hoveredInteraction = interaction;
    
    if(interactionSettings.hoveredInteraction !== undefined)
      interactionSettings.hoveredInteraction.material = new MeshBasicMaterial({color: 0x00ff00, transparent: false, wireframe:true});
  }
}

function createInteractions() {
  interactionSettings.interactions = new Group();
  interactionSettings.cameraOrigin = new Vector3(camera.position.x, camera.position.y, camera.position.z);
  interactionSettings.cameraPosition = new Vector3(camera.position.x, camera.position.y, camera.position.z);
  interactionSettings.infoDiv = document.querySelector('#info');

  sceneRoot.add(interactionSettings.interactions);
  
  let mat = new MeshBasicMaterial({color: 0x00ff00, transparent: true, opacity: 0.0});
  
  let i1 = new Mesh(new BoxGeometry(55, 55, 85), mat);
  i1.position.set(75, -95, -15);
  i1.rotation.set(0, MathF.DEG2RAD * 45, 0);
  interactionSettings.interactions.add(i1);

  getProjects(i1);
  
  let i2 = new Mesh(new BoxGeometry(95, 55, 55), mat);
  i2.position.set(-95, -95, -15);
  i2.rotation.set(0, MathF.DEG2RAD * 45, 0);
  interactionSettings.interactions.add(i2);
  i2['info'] = '<h2>Chill stack</h2>';
  
  let i3 = new Mesh(new BoxGeometry(55, 95, 55), mat);
  i3.position.set(-5, -75, -95);
  i3.rotation.set(0, MathF.DEG2RAD * 45, 0);
  i3['info'] = '<h2>Data stack</h2>JSON<br>XML';
  interactionSettings.interactions.add(i3);
}

async function getProjects(interaction) {
  let url = "https://gitlab.com/api/v4/projects?access_token=glpat-sNQWkoZ2XsMRxy-8g8zn&owned=true&simple=true";
  let response = await fetch(url);

  if (response.ok) { // if HTTP-status is 200-299
    // get the response body (the method explained below)
    let json = await response.json();
    let techInfo = "Repositories<br>";

    json.forEach((p) => {
      techInfo += "<a class=\"l\" target=\"_blank\" href=" + p.http_url_to_repo + ">";
      techInfo += p.name;
      techInfo += "</a>";
      techInfo += "<br>"
    });
  
    interaction['info'] = techInfo;
  } else {
    alert("HTTP-Error: " + response.status);
  }
}

function checkHighlightInteractions() {
  
  let raycaster = new Raycaster();
  raycaster.setFromCamera({x: mouse.x, y: mouse.y}, camera);

  let intersects = raycaster.intersectObjects(interactionSettings.interactions.children);

  if(intersects !== null && intersects.length > 0) {
    handleInteractionHover(intersects[0].object);
  } else { 
    handleInteractionHover(undefined);
  }
}

//#endregion

//#region MOUSE HANDLERS

function onMouseMove(event) {
  event.preventDefault();

  raycastTimer += deltaTime;

  if(raycastTimer > raycastDelay) {
    raycastTimer = 0.0;

    checkHighlightInteractions();
  }

  if(interactionSettings.currentInteraction !== undefined) {
    return;
  }

  mouse.x = (event.clientX / window.innerWidth) *  2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
}

function onMouseClick(event) {
  // event.preventDefault(); 

  let x = (event.clientX / window.innerWidth) *  2 - 1;
  let y = -(event.clientY / window.innerHeight) * 2 + 1;

  let raycaster = new Raycaster();
  raycaster.setFromCamera({x: x, y: y}, camera);

  let intersects = raycaster.intersectObjects(interactionSettings.interactions.children);

  if(intersects !== null && intersects.length > 0) {
    
    // if(debug_raycast === undefined){
    //   let cube = new BoxGeometry(5, 5, 5);
    //   let mat = new MeshBasicMaterial({color: 0xffffff});
    //   debug_raycast = new Mesh(cube, mat);
    //   scene.add(debug_raycast);
    // }

    // let pos = intersects[0].point;

    // debug_raycast.position.set(pos.x, pos.y, pos.z);

    handleInteractionClick(intersects[0].object);
  } else { 
    handleInteractionClick(undefined);
  }
}

//#endregion

function update() {
  requestAnimationFrame(update);

  deltaTime = clock.getDelta();

  time += deltaTime;
  particlesShader.uniforms.time.value = time;

  sceneRoot.rotation.y = 3.14 * 0.01 * mouse.x;
  sceneRoot.rotation.x = 3.14 * 0.01 * -mouse.y;

  if(roomAnimation !== undefined){
    roomAnimation.update(deltaTime);
  }

  handleCameraPosition();

  PP.render();
}

// Main Loop

start();
update();
