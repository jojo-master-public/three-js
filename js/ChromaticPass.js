function ChromaticShader() {
    let chromaticShader = {
        uniforms: {
			tDiffuse: {
				value: null
			},
            time: {
                value: 0
            },
            shiftPower: {
                value: 2.81
            },
            shiftStep: {
                value: 0.508
            },
            shiftMult: {
                value: 0.045
            }
        },
        vertexShader:
        /* glsl */
        `
            varying vec2 vUv;

            void main() {
                vUv = uv;
                gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
            }
            
        `,
        fragmentShader:
        /* glsl */
        `
            vec2 ShiftUV(vec2 uv, vec2 dir, float intensity) {
                return uv + vec2(dir.x * intensity, dir.y * intensity);
            }
            
            varying vec2 vUv;
            uniform float time;
            uniform sampler2D tDiffuse;
            uniform float shiftPower;
            uniform float shiftStep;
            uniform float shiftMult;

            void main() {
                float uvR = pow(length(vec2(0.5, 0.5) - vUv), shiftPower) * shiftMult;
                float uvG = pow(length(vec2(0.5, 0.5) - vUv), shiftPower - shiftStep) * shiftMult;
                float uvB = pow(length(vec2(0.5, 0.5) - vUv), shiftPower - shiftStep - shiftStep) * shiftMult;

                vec2 shiftDir = normalize(vec2(0.5, 0.5) - vUv);
                float r = texture2D(tDiffuse, ShiftUV(vUv, shiftDir, uvR)).r;
                float g = texture2D(tDiffuse, ShiftUV(vUv, shiftDir, uvG)).g;
                float b = texture2D(tDiffuse, ShiftUV(vUv, shiftDir, uvB)).b;
                vec3 c = vec3(r, g, b);

                vec2 uv2 = vUv + length(vec2(0.5, 0.5) - vUv) * (0.1 * sin(time));
                // gl_FragColor = texture2D( tDiffuse, uv2) * intensity;
                // gl_FragColor = vec4(shiftDir.x, shiftDir.y, 0, 1);
                gl_FragColor = vec4(c, 1.0);
            }

        `
    };

    return chromaticShader;
};

export { ChromaticShader }