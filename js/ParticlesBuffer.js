import { AdditiveBlending, Color, ShaderMaterial } from "three";

function ParticlesShader() {
    let shader = new ShaderMaterial ({
        blending: AdditiveBlending,
        depthTest: false,
        transparent: true,
        uniforms: {
            pColor: {
                value: [0.5, 0.5, 0.5]
            },
            time: {
                value: 0.0
            }
        },
        vertexShader:
        /* glsl */
        `
            vec3 AnimatedPosition(int Id, vec3 p, float Time) {
                
                float s = sin(Time + float(Id) * 0.1);
                vec3 pos = vec3(p.x + s, p.y, p.z);

                return pos;
            }

            attribute int id;

            uniform vec3 pColor;
            uniform float time;

			varying vec3 vColor;
            varying float t;
            varying float pId;



			void main() {

                t = time;
                pId = float(id);
				vColor = pColor.xyz;
                vec3 p = AnimatedPosition(id, position.xyz, time);

				vec4 mvPosition = modelViewMatrix * vec4( p, 1.0 );

				gl_PointSize = 2.0 * ( 300.0 / -mvPosition.z );
				gl_Position = projectionMatrix * mvPosition;
			}
            
        `,
        fragmentShader:
        /* glsl */
        `
            float AnimatedAlpha(float id, float time) {
                return clamp(sin(id * 0.1 + time), 0.0, 1.0);
            }

            varying float pId;
            varying float t;
            varying vec3 vColor;

            void main() {
                float alpha = AnimatedAlpha(pId, t);
                // float alpha = 1.0;

                gl_FragColor = vec4( vColor, alpha );
            }

        `
    });

    return shader;
};

export { ParticlesShader }