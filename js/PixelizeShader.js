function PixelizeShader() {
    let shader = {
        uniforms: {
			tDiffuse: {
				value: null
			},
            screenWidth: {
                value: 1920.0
            },
            screenHeight: {
                value: 1080.0
            },
            size: {
                value: 350.0
            }
        },
        vertexShader:
        /* glsl */
        `
        
            varying vec2 vUv;

            void main() {
                vUv = uv;
                gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
            }
            
        `,
        fragmentShader:
        /* glsl */
        `
            vec2 PixelizeUV(vec2 UV, float screenWidth, float screenHeight, float size)
            { 
                vec2 pixelatedUV = UV;

                float heightToWidth = screenHeight / screenWidth;
                float widthToHeight = screenWidth / screenHeight;

                pixelatedUV.x *= (size * widthToHeight);
                pixelatedUV.y *= size;

                pixelatedUV.x = round(pixelatedUV.x);
                pixelatedUV.y = round(pixelatedUV.y);

                pixelatedUV.x *= heightToWidth;

                pixelatedUV.x /= size;
                pixelatedUV.y /= size;

                return pixelatedUV;
            }

            varying vec2 vUv;
            uniform sampler2D tDiffuse;
            uniform float screenWidth;
            uniform float screenHeight;
            uniform float size;

            void main() {
                vec2 pUV = PixelizeUV(vUv, screenWidth, screenHeight, size);
                vec4 c = texture2D(tDiffuse, pUV);
                gl_FragColor = c;
                // gl_FragColor = vec4(pUV.x, pUV.y, 0.0, 1.0);
            }

        `
    };

    return shader;
};

export { PixelizeShader }